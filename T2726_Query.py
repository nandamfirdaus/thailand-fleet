import pandas as pd
from happy_query import DWHConnector
import pdb
spree_connector = DWHConnector()

qt = "with " \
     "ordernumb as ( " \
     "select " \
     "a.order_number as order_number, " \
     "so.id as order_id, " \
     "min(a.start_time) as viewed_start_time, " \
     "min(a.end_time) as viewed_end_time " \
     "from master.athena_ddf_available_slots a " \
     "left join ebdb.spree_orders so on so.number = a.order_number " \
     "WHERE a.country_id = 3 " \
     "and (available_slot_created_at+ INTERVAL '7 hours')::date >= '2020-01-01' " \
     "and (available_slot_created_at+ INTERVAL '7 hours')::date < '2020-04-01' " \
     "group by 1,2 "\
     "HAVING min(a.start_time) BETWEEN '2020-01-01' AND '2020-04-01'"\
     ") " \
     "select orn.*, " \
     "ol.order_delivery_time as selected_start_time, ol.slot_end_time as selected_end_time, " \
     "so.total as potential_total, ol.total as actual_checked_out_total " \
     "from ordernumb orn " \
     "left outer join master.order_level ol " \
     "on orn.order_number=ol.order_number " \
     "left outer join ebdb.spree_orders so " \
     "on orn.order_number=so.number "
data = spree_connector.query(qt)
print("query done")
pdb.set_trace()
data['potential_total'][data['potential_total'] < 0] = 0
data['actual_checked_out_total'][data['actual_checked_out_total'] < 0] = 0
jam = []
bulan = []
hari = []
dataselect = []
pdb.set_trace()
for index, row in data.iterrows():
    if (pd.isnull(data.at[index,'selected_start_time']))==True:
        time = pd.to_datetime(data.iloc[index]['viewed_start_time'])
        jam.append(time.hour)
        bulan.append(time.month)
        hari.append(time.strftime('%A'))
        dataselect.append(int('0'))
    else:
        time = pd.to_datetime(data.iloc[index]['selected_start_time'])
        jam.append(time.hour)
        bulan.append(time.month)
        hari.append(time.strftime('%A'))
        dataselect.append(int('1'))
pdb.set_trace()
set_time = pd.DataFrame({'hour':jam, 'day_name':hari, 'month':bulan, 'is_time_select':dataselect})
data = pd.concat([data, set_time], axis=1, sort=False)
pdb.set_trace()
indexNames = data[data['month'] > 3 ].index
data.drop(indexNames , inplace=True)
data.to_csv('T2726_Data_Jan_to_March.2020')
print("all done")
